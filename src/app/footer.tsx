export default function Footer() {
  return (
    <main className="bottom-0">
      <div className="flex-auto text-center m-10">
        <a className="p-3" href="https://devjapan.jp">
          Main
        </a>{" "}
        |{" "}
        <a className="p-3" href="https://staging.devjapan.jp">
          Staging
        </a>{" "}
        |{" "}
        <a className="p-3" href="https://develop.devjapan.jp">
          Develop
        </a>
      </div>
      <div key="copyright" className="text-xs flex-auto text-center m-10">
        Copyright © 2024 Dev Japan. All rights reserved.
      </div>
    </main>
  );
}
