import { StaticImport } from 'next/dist/shared/lib/get-img-props';
import DevJapan from "../../../../public/devjapan.jpeg";
// import DevJapan from "../../public/devjapan.jpeg";
import Image from 'next/image';
import Link from 'next/link';
import { Key } from 'react';

// interface Props {
//   images: {
//     id: string,
//     src: {
//       landscape: string,
//       large: string,
//       large2x: string,
//       medium: string,
//       original: string,
//       portrait: string,
//       small: string,
//       tiny: string,
//     },
//     alt: string,
//     width: number,
//     height: number,
//   }[];
// }

export default function ImageGallery({ images }:any) {
  return (
    <div className='p-3 grid grid-cols-[repeat(auto-fill,_minmax(500px,_1fr))] auto-cols-fr gap-4'>
      <div className='p-3 w-full h-full'>
        <div className='text-white p-3'>
          <p>Founded in 2015, Dev Japan is a community for people interested/involved in the tech industry of Japan. Events are held only on Saturdays. The original goal was to provide community members with an additional opportunties to connect with others outside of their regular working environments.</p>
        </div>
        <Image
          priority={true}
          placeholder="empty"
          alt="Dev Japan's Logo"
          src={DevJapan}
          width="500"
          height="500"
        />
        <a href="https://meetup.com/devjapan"><div className="rounded-lg border-white bg-white p-3 m-5 text-center">Visit Dev Japan</div></a>
      </div>
      {images.map((img: { id: Key | null | undefined; alt: string; src: { original: string | StaticImport; }; height: number; width: number; }) => (
        <div className='overflow-hidden w-full relative' key={img.id}>
          <Image
            key={img.id}
            priority={false}
            alt={img.alt}
            src={img.src.original}
            width={img.width}
            height={img.height}
          />
        </div>
      ))}
    </div>
  );
}