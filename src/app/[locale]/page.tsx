'use client';

import { useTranslations } from 'next-intl';
import { useState, useEffect } from 'react';
import Footer from '../footer';
import Header from '../header';
import ImageGallery from '../components/ImageGallery';
import { createClient } from 'pexels';
import { fetchMeetupPhotos } from '@/utils/api';

const client = createClient('zL10VbLGmfUOM1NqVn4yvzRrOZZXJyqnRoG2Gdp7C2Z5AJcPmXs44gho');

// All requests made with the client will be authenticated

const apiKey = process.env.MEETUP_API_KEY;
const groupName = 'devjapan';
const apiUrl = `https://api.meetup.com/${groupName}/events?key=${apiKey}`;
const query = 'Japan Tech';

interface Props {
  image: {
    id: number,
    src: { 
      original: string; 
      large2x: string; 
      large: string; 
      medium: string; 
      small: string; 
      portrait: string; 
      landscape: string; 
      tiny: string; 
    },
    alt: string | null,
    width: number,
    height: number,
  };
}

export default function Index() {
  const t = useTranslations('Index');
  const [images, setImages] = useState<Props["image"][]>([]);

  useEffect(() => {
    const getPhotos = async () => {
      try {
        const response = await client.photos.search({ query, per_page: 11 });

        if ('photos' in response) {
          setImages(response.photos);
        } else {
          console.log("Error: No photos in response");
        }
      } catch(error) {
        console.error('Error: ', error);
      }
    }

    const getMeetupPhotos = async () => {
      try {
        const response = await fetchMeetupPhotos('devjapan');

        console.log('Meetup Response: ', response);
      } catch (error) {
        console.error('Error: ', error);
      }
    }

    // getMeetupPhotos();
    getPhotos();
  }, []);

  return (
    <div className="flex">
      <main className="bg-black">
        <h1 className='text-white '>{t('title')}</h1>
        {/* <Header /> */}
        {/* <div className="flex justify-left p-3">
          <Image
            priority={true}
            placeholder="empty"
            alt="Dev Japan's Logoo"
            src={DevJapan}
            width="500"
            height="500"
          />
        </div> */}
        <ImageGallery images={images} />
        <div className="flex justify-center p-3">
          <Footer />
        </div>
      </main>
    </div>
  );
}

