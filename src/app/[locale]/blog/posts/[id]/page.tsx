import React from 'react';
import { fetchPosts } from '@/utils/api';
import { notFound } from 'next/navigation';

interface Post {
  id: string;
  title: string;
  body: string;
}

interface PostProps {
  params: {
    id: string;
  }
}

const Page = async ({ params }: PostProps) => {
  const posts = await fetchPosts();
  const post = posts.find((post: Post) => post.id.toString() === params.id);

  if (!post) {
    notFound();
  }

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">{post.title}</h1>
      <p className="text-lg">{post.body}</p>
    </div>
  );
};

export async function generateStaticParams() {
  const posts = await fetchPosts();
  return posts.map((post: Post) => ({
    id: post.id.toString(),
  }));
}

export default Page;