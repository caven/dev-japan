"use client";

import React, { useState, useEffect } from 'react';
import { fetchPosts } from '@/utils/api';
import Link from 'next/link';

interface Post {
  id: string,
  title: string,
}

export default function Blog() {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetchPosts();
        setPosts(response);
        // console.log('Response: ', response);
      } catch (error) {
        console.error('Error', error);
      }
    }

    fetchData();
  }, []);

  // console.log("Posts from Blog: ", posts);
  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">Blog Posts</h1>
      <ul>
        {posts.map((post) => (
          <li key={post.id} className="mb-4">
            <Link href={`/blog/posts/${post.id}`} className="text-blue-600 hover:underline text-xl">
              {post.title}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}