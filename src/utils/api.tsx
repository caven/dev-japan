const API_KEY = '9c82fnkrja6ubl807do2fdjd0i';
const BASE_URL = 'https://api.meetup.com';

export const fetchMeetupPhotos = async(groupUrlName: string) => {
  const response = await fetch(`${BASE_URL}/${groupUrlName}/photos?key=${API_KEY}`);
  const data = await response.json();
  return data;
};

export const fetchPosts = async() => {
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');

  if (!response.ok) {
    throw new Error('Failed to fetch posts');
  }
  const data = await response.json();
  return data;
}