import createNextIntlPlugin from 'next-intl/plugin';

const withNextIntl = createNextIntlPlugin();

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "images.pexels.com" , // allows images from pexels to be used in <Image />
      },
      {
        protocol: "https",
        hostname: "images.unsplash.com" , // allows images from unsplash to be used in <Image />
      }
    ]
  }
};

export default withNextIntl(nextConfig);
